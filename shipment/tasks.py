from random import choice
import logging

from celery import shared_task

from shipment.models import Shipment, StatusChoices


@shared_task()
def place_shipment_into_container(shipment_id: int):
    try:
        shipment = Shipment.objects.get(pk=shipment_id)
    except Shipment.DoesNotExist:
        logging.info(f"Shipment with id {shipment_id} does not exist")
        return
    shipment.status = StatusChoices.IN_CONTAINER
    shipment.save(update_fields=["status"])


@shared_task()
def finalize_shipment(shipment_id: int):
    statuses = (StatusChoices.RECEIVED, StatusChoices.REJECTED)
    try:
        shipment = Shipment.objects.get(pk=shipment_id)
    except Shipment.DoesNotExist:
        logging.info(f"Shipment with id {shipment_id} does not exist")
        return
    shipment.status = choice(statuses)
    shipment.save(update_fields=["status"])
