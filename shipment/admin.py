from django.contrib import admin

from shipment.models import Shipment


@admin.register(Shipment)
class ShipmentAdmin(admin.ModelAdmin):

    list_display = (
        "weight",
        "cost",
        "status",
        "origin",
        "destination",
        "created_at",
        "updated_at",
    )
    ordering = ("-created_at",)
