from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
)

from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import (
    CreateModelMixin,
    ListModelMixin,
    UpdateModelMixin,
    RetrieveModelMixin,
)

from shipment.models import Shipment
from shipment.serializers import ShipmentModelSerializer, ShipmentCreateSerializer


class ShipmentListView(ListView):
    model = Shipment
    template_name = "shipment_list.html"
    context_object_name = "shipments"


class ShipmentCreateView(CreateView):
    model = Shipment
    template_name = "shipment_form.html"
    fields = ["weight", "cost", "origin", "destination"]
    success_url = reverse_lazy(
        "shipment-list"
    )  # URL to redirect after successful creation


class ShipmentUpdateView(UpdateView):
    model = Shipment
    fields = ["weight", "cost", "status", "origin", "destination"]
    template_name = "shipment_form.html"

    success_url = reverse_lazy(
        "shipment-list"
    )  # URL to redirect after successful update


class ShipmentViewSet(
    ListModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    RetrieveModelMixin,
    GenericViewSet,
):
    queryset = Shipment.objects.all()

    def get_serializer_class(self):
        if self.request.method in ("POST", "PUT"):
            return ShipmentCreateSerializer
        return ShipmentModelSerializer
