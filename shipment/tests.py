from unittest.mock import patch

from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase

from shipment.models import Shipment
from shipment.tasks import place_shipment_into_container, finalize_shipment


class ShipmentTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.shipment = Shipment.objects.create(
            weight=1,
            cost=1,
            origin="Kyiv",
            destination="Lviv",
        )

    def test_shipment_listview(self):
        response = self.client.get(reverse("shipment-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Shipment.objects.count(), 1)
        self.assertContains(response, self.shipment.origin)
        self.assertContains(response, self.shipment.destination)

    def test_shipment_createview(self):
        response = self.client.post(
            reverse("shipment-create"),
            data={"weight": 1, "cost": 1, "origin": "Kyiv", "destination": "Lviv"},
        )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(Shipment.objects.count(), 2)

    def test_shipment_updateview(self):
        response = self.client.post(
            reverse("shipment-update", kwargs={"pk": self.shipment.tracking_number}),
            data={
                "weight": 200,
                "cost": 200,
                "status": "New",
                "origin": "Kyiv",
                "destination": "Dubai",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(Shipment.objects.count(), 1)
        updated_shipment = Shipment.objects.get(pk=self.shipment.tracking_number)
        self.assertEqual(updated_shipment.weight, 200)
        self.assertEqual(updated_shipment.cost, 200)
        self.assertEqual(updated_shipment.status, "New")
        self.assertEqual(updated_shipment.origin, "Kyiv")
        self.assertEqual(updated_shipment.destination, "Dubai")

    @patch("shipment.tasks.place_shipment_into_container.apply_async")
    def test_pre_save_triggered(self, apply_async):
        response = self.client.post(
            reverse("shipment-create"),
            data={"weight": 1, "cost": 1, "origin": "Kyiv", "destination": "Lviv"},
        )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertEqual(Shipment.objects.count(), 2)
        self.assertEqual(apply_async.call_count, 1)

    def test_status_change(self):
        shipment = Shipment.objects.create(
            weight=1,
            cost=1,
            origin="Kyiv",
            destination="Lviv",
        )
        place_shipment_into_container(shipment.pk)
        shipment.refresh_from_db()
        self.assertEqual(shipment.status, "In container")
        finalize_shipment(shipment.pk)
        shipment.refresh_from_db()
        self.assertIn(shipment.status, ("Received", "Rejected"))


class APITests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.shipment = Shipment.objects.create(
            weight=1,
            cost=1,
            origin="Kyiv",
            destination="Lviv",
        )

    def test_api_listview(self):
        response = self.client.get(reverse("shipment-api-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Shipment.objects.count(), 1)
        self.assertContains(response, self.shipment.origin)
        self.assertContains(response, self.shipment.destination)

    def test_api_detailview(self):
        response = self.client.get(
            reverse(
                "shipment-api-detail", kwargs={"pk": self.shipment.tracking_number}
            ),
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Shipment.objects.count(), 1)
        self.assertContains(response, self.shipment.origin)
        self.assertContains(response, self.shipment.destination)
        self.assertEqual(response.data.get("status"), "New")

    @patch("shipment.tasks.place_shipment_into_container.apply_async")
    def test_api_createview(self, apply_async):
        response = self.client.post(
            reverse("shipment-api-list"),
            data={"weight": 1, "cost": 1, "origin": "Kyiv", "destination": "Lviv"},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Shipment.objects.count(), 2)
        self.assertEqual(response.data.get("status"), "New")
        self.assertEqual(apply_async.call_count, 1)

    def test_api_updateview(self):
        response = self.client.patch(
            reverse(
                "shipment-api-detail", kwargs={"pk": self.shipment.tracking_number}
            ),
            data={"cost": 100},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Shipment.objects.count(), 1)
        self.assertEqual(response.data.get("cost"), 100)
