import logging

from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from shipment.models import Shipment, StatusChoices
from shipment.tasks import place_shipment_into_container, finalize_shipment
from logistic_service.settings import STATUS_CHANGE_TIME


@receiver(pre_save, sender=Shipment)
def pre_save_shipment(sender, instance, **kwargs):
    try:
        old_instance = Shipment.objects.get(pk=instance.pk)
    except Shipment.DoesNotExist:
        logging.info(f"New shipment, skip pre_save")
        return

    if old_instance.status != instance.status:
        logging.info(
            f"Shipment {instance.pk} status changed from {old_instance.status} to {instance.status}"
        )
        if instance.status == StatusChoices.NEW:
            place_shipment_into_container.apply_async(
                args=(instance.pk,), countdown=STATUS_CHANGE_TIME
            )
            logging.info(
                f"Shipment {instance.pk}, place_shipment_into_container task scheduled"
            )
        if instance.status == StatusChoices.IN_CONTAINER:
            finalize_shipment.apply_async(
                args=(instance.pk,), countdown=STATUS_CHANGE_TIME
            )
            logging.info(f"Shipment {instance.pk}, finalize_shipment task scheduled")


@receiver(post_save, sender=Shipment)
def post_save_shipment(sender, created, instance, **kwargs):
    if created:
        place_shipment_into_container.apply_async(
            args=(instance.pk,), countdown=STATUS_CHANGE_TIME
        )
        logging.info(
            f"New shipment {instance.pk}, place_shipment_into_container task scheduled"
        )
