from django.db import models


class StatusChoices(models.TextChoices):
    NEW = "New"
    IN_CONTAINER = "In container"
    RECEIVED = "Received"
    REJECTED = "Rejected"


class Shipment(models.Model):
    tracking_number = models.BigAutoField(primary_key=True)
    status = models.CharField(
        choices=StatusChoices.choices, default=StatusChoices.NEW, max_length=15
    )
    weight = models.FloatField(blank=True, null=True)
    cost = models.FloatField(blank=True, null=True)
    origin = models.CharField(verbose_name="From", max_length=255)
    destination = models.CharField(verbose_name="To", max_length=255)
    created_at = models.DateTimeField(
        verbose_name="Created at", auto_now_add=True, editable=False, null=True
    )
    updated_at = models.DateTimeField(
        verbose_name="Updated at", auto_now=True, editable=False, null=True
    )

    def __str__(self):
        return f"{self.tracking_number} - {self.status}"
