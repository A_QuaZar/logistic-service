from django.urls import path, include
from rest_framework import routers

from shipment.views import (
    ShipmentListView,
    ShipmentCreateView,
    ShipmentUpdateView,
    ShipmentViewSet,
)

router = routers.SimpleRouter(trailing_slash=False)
router.register("", ShipmentViewSet, basename="shipment-api")

urlpatterns = [
    path("", ShipmentListView.as_view(), name="shipment-list"),
    path("create/", ShipmentCreateView.as_view(), name="shipment-create"),
    path("<int:pk>/update/", ShipmentUpdateView.as_view(), name="shipment-update"),
    path("api/", include(router.urls), name="shipment-api"),
]
