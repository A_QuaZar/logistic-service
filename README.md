# Simple service for Shipment Logistsics

## Starting service

### Environment variables
- **SECRET_KEY** - Django secret key 
- **DEBUG** - Debug mode
- **CELERY_BROKER_URL** - Url for celery broker (Used redis)
- **STATUS_CHANGE_TIME** - Time in seconds to change status of shipment

### Localy
* Copy `.env.example` to `.env` and fill it with your data 
  *(Note: Use `CELERY_BROKER_URL=redis://127.0.0.1:6379/0`)* 
* Create virtual environment: `python -m venv venv` and activate it `source venv/bin/activate`
* Install dependencies: `pip install -r requirements.txt`
* Make migrations: `python manage.py makemigrations`
* Migrate: `python manage.py migrate`
* Run server: `python manage.py runserver 0.0.0.0:8000`
* Run redis: `redis-server` *(Install redis in your machine if needed)*
* Run celery worker: `celery -A logistic_service worker -l INFO`

### Via Docker
* Copy `.env.example` to `.env` and fill it with your data
  *(Note: Use `CELERY_BROKER_URL=redis://redis:6379/0`)*
* To build docker: `docker-compose build`
* To run services: `docker-compose up`

### URLS
**Web forms**
* List of shipments: `http://127.0.0.1:8000/shipments/`
* Create shipment: `http://127.0.0.1:8000/shipments/create/`
* Update shipment: `http://127.0.0.1:8000/shipments/<id>/update/`

**API**
* Api for shipments list: `http://127.0.0.1:8000/shipments/api/` (POST method to create shipment)
* Api for details of shipment: `http://127.0.0.1:8000/shipments/api/<id>` (PUT method to change shipment)

### Tests

* To run tests locally: `python manage.py test`
* To run tests via docker: `exec logistic_service_web_1 python manage.py test` (While service is running)


